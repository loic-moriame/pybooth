import gpiozero


class Button(gpiozero.Button):
    def __init__(
        self,
        pin=None,
        pull_up=True,
        bounce_time=None,
        hold_time=1,
        hold_repeat=False,
        pin_factory=None,
    ):
        self.button = super(Button, self).__init__(
            pin, pull_up, bounce_time, hold_time, hold_repeat, pin_factory
        )
        self.__is_enable = True

    def is_enable(self):
        """ return current state of the button """
        return self.__is_enable

    def state(self):
        """ display current state of the button """
        print(
            ">>> button state: {0}".format("enable" if self.__is_enable else "disable")
        )

    def enable(self):
        """ change current state of the button to ENABLE """
        self.__is_enable = True

    def disable(self):
        """ change current state of the button to DISABLE """
        self.__is_enable = False
