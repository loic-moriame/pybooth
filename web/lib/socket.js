const fs = require('fs');
const path = require('path');

const io = require('socket.io')();
const watch = require('node-watch');

const debug = require('debug')('pibooth:socket');
debug('load lib/socket.js');
let nb_files = 0;

io.on('connection', async socket => {
  debug('user joined');
  io.emit('users:update', io.engine.clientsCount);

  (() => {
    nb_files = fs.readdirSync('/media/usb/thumbnail').filter(file => {
      return path.extname(file).match(/\.(jpg|jpeg)/i);
    }).length;

    io.emit('pictures:update', nb_files);
  })();

  socket.on('disconnect', () => {
    debug('user left');
    io.emit('users:update', io.engine.clientsCount);
  });
});

watch(path.resolve('/media', 'usb', 'thumbnail'), { filter: /\.jpg|jpeg$/ }, (evt, file) => {
  debug(evt, file);
  if (evt == 'update') {
    nb_files++;
    file = path.basename(file);
    const img = {
      name: file,
      src: path.join('photos', file),
      thumbnail: path.join('photos', 'thumbnail', file)
    };

    io.emit('picture:new', img);
    debug(`io.emit("picture:new", ${img})`);

    io.emit('pictures:update', nb_files);
    debug(`io.emit("picture:new", ${JSON.stringify(nb_files)})`);
  }
  if (evt == 'remove') {
    nb_files--;

    file = path.basename(file);
    const img = {
      name: file,
      src: path.join('photos', file),
      thumbnail: path.join('photos', 'thumbnail', file)
    };

    io.emit('picture:delete', img);
    debug(`io.emit("picture:new", ${img})`);

    io.emit('pictures:update', nb_files);
    debug(`io.emit("picture:new", ${JSON.stringify(nb_files)})`);
  }
});

module.exports = io;
