const fs = require('fs');
const path = require('path');

const express = require('express');
const router = express.Router();
const debug = require('debug')('pibooth:router:home');
debug('load routes/home');

router.get('/', (req, res, next) => {
  const photos = getPhotos('/media/usb/thumbnail');
  res.render('home/index', { title: 'Pybooth', photos });
});

function getPhotos(dir) {
  return fs
    .readdirSync(dir)
    .filter(file => {
      return path.extname(file).match(/\.(jpg|jpeg)/i);
    })
    .map(file => {
      return {
        name: file,
        src: path.join('photos', file),
        thumbnail: path.join('photos', 'thumbnail', file),
        time: fs.statSync(`${dir}/${file}`).mtime.getTime()
      };
    })
    .sort((a, b) => {
      return b.time - a.time;
    });
}

module.exports = router;
