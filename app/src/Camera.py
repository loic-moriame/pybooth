import glob
import os
import subprocess
import tempfile
import time

import gpiozero
from PIL import Image

from src.Logger import logger
from src.utils import ROOT_DIR


class Camera:
    def __init__(self, App):
        self.app = App
        self.relay = None
        self.state = None

        pin_relay = App.config.get("Camera", "pin_relay")
        if pin_relay:
            self.relay = gpiozero.OutputDevice(
                pin_relay, active_high=False, initial_value=False
            )
            self.state = self.relay.value

        self.save_path = "/media/usb"

        self.tmp_dir = tempfile.mkdtemp()
        self.tmp_file = os.path.join(self.tmp_dir, "fifo.mjpg")
        os.mkfifo(self.tmp_file)

        self.watermark = False
        self.overlay = False
        if 1 == App.config.get("Pybooth", "watermark"):
            try:
                watermark = os.path.join(ROOT_DIR, "assets/images/watermark.png")
                overlay = os.path.join(ROOT_DIR, "assets/images/overlay.png")
                self.watermark = Image.open(watermark)
                Image.open(overlay)
                self.overlay = overlay
            except IOError:
                self.watermark = False
                self.overlay = False

        self.poweroff()
        self.poweron()
        time.sleep(5)

    @property
    def is_poweron(self):
        return self.relay.value

    def poweron(self):
        self.relay.on()
        time.sleep(0.5)

    def poweroff(self):
        self.relay.off()
        time.sleep(0.5)

    def start_liveview(self):
        if not self.is_poweron:
            self.poweron()
            time.sleep(3)

        cmd = "gphoto2 --capture-movie --stdout > {0} &".format(self.tmp_file)
        p = subprocess.Popen(cmd, shell=True)
        p.wait()

        cmd = "omxplayer {0} --live --layer 10 --win '160, 0, 1120, 600' --no-keys &".format(
            self.tmp_file
        )
        p = subprocess.Popen(cmd, shell=True)
        p.wait()
        time.sleep(1)

        #   start overlay
        if self.overlay:
            cmd = "pngview -b 0 -l 50 {0} -x 160 -y 490 &".format(self.overlay)
            p = subprocess.Popen(cmd, shell=True)
            p.wait()

    def stop_liveview(self):
        #   Kill pngview
        if self.overlay:
            cmd = "killall -s SIGKILL pngview"
            p = subprocess.Popen(cmd, shell=True)
            p.wait()

        #   Kill omxplayer & live capture
        cmd = "killall -s SIGKILL omxplayer.bin"
        p = subprocess.Popen(cmd, shell=True)
        p.wait()

        cmd = "gphoto2 --set-config viewfinder=0"
        p = subprocess.Popen(cmd, shell=True)
        p.wait()

    def shoot(self):
        self.stop_liveview()

        path_thumbnail = "{0}/thumbnail".format(self.save_path)

        files = glob.glob("{0}/*.jpg".format(path_thumbnail))
        file_no = len(files) + 1

        name = "IMG_{0:04d}.jpg".format(file_no)
        save_path = "{0}/{1}".format(self.save_path, name)
        save_path_tb = "{0}/thumbnail/{1}".format(self.save_path, name)

        cmd = "gphoto2 --capture-image-and-download --force-overwrite --filename {0} --set-config imageformat=0".format(
            save_path
        )
        logger.debug("cmd: {}".format(cmd))
        p = subprocess.Popen(cmd, shell=True)
        p.wait()

        self.app.render_text("Vous pouvez bouger : votre cliché arrive :)", zone="top")
        try:
            if self.watermark:
                logger.debug("Apply watermark")
                photo = Image.open(save_path)
                photo = photo.transpose(Image.FLIP_LEFT_RIGHT)
                photo.paste(self.watermark, self.watermark)
                photo.save(save_path, "JPEG")

            photo_tb = Image.open(save_path)
            photo_tb.thumbnail([1120, 600])
            photo_tb.save(save_path_tb, "JPEG")

        except FileNotFoundError as err:
            logger.error(err)
            pass

        self.poweroff()
        self.poweron()

        return save_path, save_path_tb
