import subprocess
from src.Logger import logger


class Printer:
    """
    Printer object

    :type App: App
    :param App:
        Instance of Pybooth's App

    :type name: str
    :param name:
        Name of the printer (cf. CUPS)
    """

    def __init__(self, App, name):
        self.app = App
        self.name = name

    def print_file(self, file):
        """ Print a file with the current printer """
        cmd = "lp -d {0} {1}".format(self.name, file)
        p = subprocess.Popen(cmd, shell=True)
        p.wait()
        return
