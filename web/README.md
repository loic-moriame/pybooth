# Notes

## Disable screen blanking

`/home/pi/.config/lxsession/LXDE-pi/autostart`
```
@xset s noblank
@xset s off
@xset -dpms
```

## Disable mouse pointer

```
sudo apt install -y unclutter
```

`/home/pi/.config/lxsession/LXDE-pi/autostart`
```
@unclutter -idle 0
```

source : https://jackbarber.co.uk/blog/2017-02-16-hide-raspberry-pi-mouse-cursor-in-raspbian-kiosk

## Rapsbian Stretch power-off button
Raspbian stretch 2017.08.16 ou plus récent

`/boot/config.txt`

```
dtoverlay=gpio-shutdown
```
Connecter un bouton sur le port GPIO3 et GND : à chaque pression, la rapsberry sera éteinte.
Si elle est déjà éteinte, ça sera un signal pour la redémarrer.

source : https://dzone.com/articles/making-your-own-rpi-power-button

# Install gphoto2

`wget https://raw.githubusercontent.com/gonzalo/gphoto2-updater/master/gphoto2-updater.sh && chmod +x gphoto2-updater.sh && sudo ./gphoto2-updater.sh`

app

> state : permet de définir à quelle étape du workflow nous sommes (waiting / shooting / validate print / printing)

Class Button >> GPIO
state (available/disable) : permet de couper le bouton poussoir suivant les étapes.
on press >> socket.io emit events (variable suivant l'étapes à l'aquelle on est)

* button:take_picture
* button:validate_print

Class Camera >> gphoto2
shoot() : prend une photo, la copie en locale
makePhotoMontage(photo) : fais un photo montage avec l'image passée en paramètre

Class Printer >> CUPS
print(picture) : imprime une photo

.env
GPHOTO2_DIR : <string> répertoire où seront copiées les images
GPHOTO2_TIMESTAMPED : <boolean> permet de définir si les photos seront renomées suivant l'heure de prise où suivant leur index

GPIO_PIN_BUTTON : <integer> permet de définir le pin GPIO sur lequel est connecté le bouton poussoir

mjpeg_streamer command:
/usr/local/bin/mjpg_streamer -i "/usr/local/lib/mjpg-streamer/input_raspicam.so -x 1080 -y 720 -hf -vf" -o "/usr/local/lib/mjpg-streamer/output_http.so -w /usr/local/share/mjpg-streamer/www"

## PM2

Installation :
```
npm install pm2 -g
```

Complete your installation with the CLI autocompletion :
```
pm2 completion install
```

## Lancement automatique au démarrage

```/home/pi/.config/lxsession/LXDE-pi/autostart
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
@point-rpi

## Reset Chromium crash state (in case of hard reboot)
@/bin/cp /home/pi/pibooth/config/chromium/Preferences /home/pi/.config/chromium/Default/
@/bin/cp '/home/pi/pibooth/config/chromium/Local State' /home/pi/.config/chromium/

## Disable screen off / blank
#@xscreensaver -no-oplash
@xset s off
@xset -dpms
@xset s noblank

# Remove cursor
@unclutter -idle 0.1 -root

## Start Chromium with PiBooth screen launched
@/usr/bin/chromium-browser --kiosk --noerrdialogs --disable-infobars http://pibooth.local/
```
