from src.Logger import logger


class State:

    NONE = {"ID": "NONE", "label": ""}
    WAITING = {"ID": "WAITING", "label": "Appuyez sur le bouton pour prendre une photo"}
    COUNTDOWN_SHOOTING = {"ID": "COUNTDOWN_SHOOTING", "label": ""}
    SHOOTING = {"ID": "SHOOTING", "label": "On ne bouge plus !"}
    CONFIRM_PRINT = {
        "ID": "CONFIRM_PRINT",
        "label": "Restez appuyé sur le bouton pour imprimer la photo",
    }
    # PRINT_CONFIRMED ="ID": "#",  {"label": ""}
    # PRINTING ="ID": "#",  {"label": "Impression en cours..."}
    PRINT_CONFIRMED = {"ID": "PRINT_CONFIRMED", "label": "Impression en cours..."}
    PRINT_END = {"ID": "PRINT_END", "label": "Admirez votre oeuvre ;)"}
    CHANGE_PAPER = {"ID": "CHANGE_PAPER", "label": "...Rechargez le papier..."}
    CHANGE_TONER = {"ID": "CHANGE_TONER", "label": "...Changez de cartouche..."}
    CHANGE_TONER_AND_PAPER = {
        "ID": "CHANGE_TONER_AND_PAPER",
        "label": "...Changez de cartouche et rechargez le papier...",
    }
    WAITING_MAINTENANCE = {"ID": "WAITING_MAINTENANCE", "label": ""}

    def __init__(self, App):
        self.app = App
        self.current_state = None
        self.check_cartbridge_or_paper = True
        self.reset_states()
        self.set_state(self.WAITING)
        self.update_current_state()

    def reset_states(self):
        self.states = []
        self.states.append(self.NONE)
        self.states.append(self.WAITING)
        self.states.append(self.COUNTDOWN_SHOOTING)
        self.states.append(self.SHOOTING)
        self.states.append(self.CONFIRM_PRINT)
        self.states.append(self.PRINT_CONFIRMED)
        self.states.append(self.PRINT_END)
        return True

    def update_current_state(self):
        total_sheets = int(self.app.config.get("Printer", "total_sheets"))
        nb_of_cartridge = int(self.app.config.get("Printer", "nb_of_cartridge"))
        nb_of_sheet = int(self.app.config.get("Printer", "nb_of_sheet"))

        cartridge_capacity = total_sheets / nb_of_cartridge
        sheet_capacity = total_sheets / nb_of_sheet

        print_left = int(self.app.config.get("Printer", "print_left"))

        #   Skip control if we start a new cartridge & papers
        if print_left >= total_sheets:
            self.current_state = self.states[0]
            return

        if self.current_state == self.WAITING_MAINTENANCE:
            self.check_cartbridge_or_paper = False
            return

        if self.current_state == self.PRINT_END:
            self.check_cartbridge_or_paper = True
            return

        if self.check_cartbridge_or_paper:
            #   reached end of all sheets
            if print_left == 0:
                self.current_state = self.CHANGE_TONER_AND_PAPER
                return

            #   reached end cartridge & paper capacity
            if (
                print_left % cartridge_capacity == 0
                and print_left % sheet_capacity == 0
            ):
                self.current_state = self.CHANGE_TONER_AND_PAPER
                return

            #   reached end of current cartridge capacity
            if print_left % cartridge_capacity == 0:
                self.current_state = self.CHANGE_TONER
                return

            #   reached end of current sheets capacity
            if print_left % sheet_capacity == 0:
                self.current_state = self.CHANGE_PAPER
                return

        #   set default value
        self.current_state = self.states[0]

    def get_current_state(self):
        self.update_current_state()
        return self.current_state

    def set_state(self, state):
        """Set the current_state to state"""
        while self.states[0] != state:
            self.states = self.states[1:] + self.states[:1]

    def next(self):
        """
        Rotate the current states list
        Usefull to get the next state

        Special states can't overide rotation:
            CHANGE_TONER
            CHANGE_PAPER
            CHANGE_TONER_AND_PAPER
            WAITING_MAINTENANCE
        """
        if (
            self.current_state == self.CHANGE_PAPER
            or self.current_state == self.CHANGE_TONER
            or self.current_state == self.CHANGE_TONER_AND_PAPER
        ):
            previous_state = self.current_state
            self.current_state = self.WAITING_MAINTENANCE
            self.current_state["label"] = previous_state["label"]
            return

        if self.current_state == self.WAITING_MAINTENANCE:
            self.current_state = self.WAITING
            return

        self.states = self.states[1:] + self.states[:1]
        self.current_state = self.states[0]

