import os
import signal
import subprocess
import time

import pygame

from src.Button import Button
from src.Camera import Camera
from src.Config import Config
from src.Logger import logger
from src.Printer import Printer
from src.State import State
from src.utils import ROOT_DIR


class App:
    def __init__(self, display):
        """
        Initialize the Pibooth application (a pygame programme in fact)
        """

        self.config = Config()

        """ Initialize Pygame """
        pygame.init()

        """ Hide mouse """
        pygame.mouse.set_visible(False)

        """ Define the screen for the game """
        self.display = display

        """ Define the font for the game """
        ubuntu_font_path = os.path.join(ROOT_DIR, "assets/fonts/Ubuntu-B.ttf")
        self.font = pygame.font.Font(str(ubuntu_font_path), 40)

        """
        Calculate areas for Pictures & Text

        The screen is split in two area :
            - one top, mostly for images preview (have proportional display)
            - one bottom, mostly to display text (width 100%)

        To calculate areas, we take the screen resolution
        and percent text display and make some maths ;)

        Then, Pygame rectangles are calculated
        """
        self.res_screen = (
            int(self.config.get("Screen", "width")),
            int(self.config.get("Screen", "height")),
        )
        screen_width = int(self.config.get("Screen", "width"))
        screen_height = int(self.config.get("Screen", "height"))
        percent_text_area_display = (
            int(self.config.get("Screen", "percent_text_area_display")) / 100
        )
        percent_preview_area_display = 1 - percent_text_area_display

        # (left, top, width, height)
        self.rect_display = 0, 0, screen_width, screen_height

        res_preview = (
            screen_width * percent_preview_area_display,
            screen_height * percent_preview_area_display,
        )

        # (left, top, width, height)
        self.rect_preview = (
            (screen_width - res_preview[0]) / 2,
            0,
            res_preview[0],
            res_preview[1],
        )
        pygame.draw.rect(display, pygame.Color("green"), self.rect_preview)

        res_text = (screen_width, screen_height * percent_text_area_display)
        # (left, top, width, height)
        self.rect_text = (0, screen_height - res_text[1], res_text[0], res_text[1])
        pygame.draw.rect(display, pygame.Color("cyan"), self.rect_text)

        if 1 == self.config.get("AP", "enable"):
            ssid = self.config.get("AP", "SSID")
            logger.info("Create AP '{0}'".format(ssid))
            #   Stop any previous AP
            cmd = "sudo create_ap --stop wlan0"
            p = subprocess.Popen(cmd, shell=True)
            p.wait()

            #   Create AP in daemon mode
            cmd = 'sudo create_ap --daemon -n wlan0 --isolate-clients "{0}"'.format(
                ssid
            )
            gateway = self.config.get("AP", "gateway")
            if gateway:
                cmd = 'sudo create_ap --daemon -n wlan0 --isolate-clients -g {0} "{1}"'.format(
                    gateway, ssid
                )
            logger.debug(cmd)
            p = subprocess.Popen(cmd, shell=True)
            p.wait()

        self.camera = Camera(self)
        self.camera.start_liveview()
        self.last_picture = None

        self.button = Button(self.config.get("Pybooth", "pin_button_action"))
        self.button.when_pressed = self.button_pressed

        power_button = Button(
            self.config.get("Pybooth", "pin_button_shutdown"), hold_time=10
        )
        power_button.when_held = self.shutdown

        self.state = State(self)
        self.state.set_state(self.state.NONE)

    def loop(self):
        print("press button...")
        self.button_pressed()

        while True:
            self.render_text(self.state.get_current_state()["label"])
            signal.pause()

    def button_pressed(self):
        if not self.button.is_enable():
            return

        self.state.next()
        # Clear previous text
        pygame.draw.rect(self.display, pygame.Color("black"), self.rect_preview)
        pygame.draw.rect(self.display, pygame.Color("black"), self.rect_text)

        if self.state.get_current_state() == self.state.WAITING:
            logger.debug("WAITING")
            self.camera.start_liveview()
            self.render_text(self.state.get_current_state()["label"])
            self.button.disable()
            time.sleep(2)
            self.button.enable()
            return

        if self.state.get_current_state() == self.state.COUNTDOWN_SHOOTING:
            logger.debug("COUNTDOWN_SHOOTING")
            self.countdown_shoot()
            self.state.next()

        if self.state.get_current_state() == self.state.SHOOTING:
            logger.debug("SHOOTING")
            self.render_text(self.state.get_current_state()["label"], zone="top")
            self.shoot()
            self.state.next()

        if self.state.get_current_state() == State.CONFIRM_PRINT:
            logger.debug("CONFIRM_PRINT")
            self.confirm_print()

        if self.state.get_current_state() == State.PRINT_CONFIRMED:
            logger.debug("PRINT_CONFIRMED")
            self.print()
            self.state.next()

        if self.state.get_current_state() == State.PRINT_END:
            logger.debug("PRINT_END")
            self.render_text(self.state.get_current_state()["label"])
            time.sleep(5)
            self.state.set_state(self.state.NONE)
            self.button_pressed()
            return

        if self.state.get_current_state() == self.state.CHANGE_TONER_AND_PAPER:
            logger.debug("CHANGE_TONER_AND_PAPER")
            self.state.set_state(self.state.NONE)
            return

        if self.state.get_current_state() == self.state.CHANGE_PAPER:
            logger.debug("CHANGE_PAPER")
            self.render_text(self.state.get_current_state()["label"])
            self.state.set_state(self.state.NONE)
            return

        if self.state.get_current_state() == self.state.CHANGE_TONER:
            logger.debug("CHANGE_TONER")
            self.render_text(self.state.get_current_state()["label"])
            self.state.set_state(self.state.NONE)
            return

        if self.state.get_current_state() == self.state.WAITING_MAINTENANCE:
            logger.debug("WAITING_MAINTENANCE")
            self.render_text(self.state.get_current_state()["label"])

            #   Reset nb of print left after changed TONER and PAPER
            print_left = int(self.config.get("Printer", "print_left"))
            if print_left <= 0:
                print_left = self.config.get("Printer", "total_sheets")
                self.config.update("Printer", "print_left", print_left)

            self.state.set_state(self.state.NONE)
            return

    def render_text(self, text="", zone="bottom", clear_all=True):
        """ Reset text on all display """
        pygame.draw.rect(self.display, pygame.Color("black"), self.rect_display)

        text_box = pygame.draw.rect(self.display, pygame.Color("black"), self.rect_text)
        if zone == "top":
            text_box = pygame.draw.rect(
                self.display, pygame.Color("black"), self.rect_preview
            )

        """ Render one line of text """
        if isinstance(text, str):
            text = self.font.render(text, True, pygame.Color("white"))

            if zone == "top":
                text_rect = text.get_rect(
                    center=(self.res_screen[0] / 2, self.res_screen[1] / 2)
                )

            if zone == "bottom":
                text_rect = text.get_rect()
                text_rect.center = text_box.center
                text_rect.top = text_box.top

            # Flip the text vertically.
            text = pygame.transform.flip(text, True, False)
            self.display.blit(text, text_rect)

        """ Render multiple lines of text """
        if isinstance(text, list):
            top = text_box.top

            for x in range(len(text)):
                render = self.font.render(text[x], True, pygame.Color("white"))
                # Flip the text vertically.
                render = pygame.transform.flip(render, True, False)
                render_rect = render.get_rect()
                render_rect.center = text_box.center
                render_rect.top = top
                self.display.blit(render, render_rect)
                top += 40  # moves the following line down 30 pixels

        # self.__update_screen()
        if clear_all:
            pygame.display.update()
        else:
            if zone == "bottom":
                pygame.display.update(self.rect_text)
            else:
                pygame.display.update(self.rect_preview)

    def countdown_shoot(self):
        timer = int(self.config.get("Pybooth", "countdown_seconds_shoot"))
        while timer:
            self.render_text("{0}".format(timer))
            time.sleep(1)
            timer -= 1

    def confirm_print(self):
        label = self.state.get_current_state()["label"]
        timer = int(self.config.get("Pybooth", "countdown_seconds_print"))

        confirmed = False
        while timer:
            if self.button.is_pressed:
                confirmed = True
                break

            mins, secs = divmod(timer, 60)
            timeformat = "{:02d}:{:02d}".format(mins, secs)
            self.render_text([label, timeformat], clear_all=False)
            time.sleep(1)
            timer -= 1

        if not confirmed:
            self.state.set_state(self.state.NONE)

        self.button_pressed()

    def shoot(self):
        try:
            photo, thumbnail = self.camera.shoot()
            self.last_picture = photo

            image = pygame.image.load(thumbnail).convert()
            image = pygame.transform.flip(image, True, False)
            self.display.blit(image, (160, 0))
            pygame.display.flip()
        except pygame.error as err:
            logger.error(err)
            pass

    def print(self):
        self.button.disable()

        printer = Printer(self, self.config.get("Printer", "name"))
        printer.print_file(self.last_picture)

        timer = int(self.config.get("Printer", "print_time_seconds"))
        if not timer:
            timer = 60

        label = self.state.get_current_state()["label"]
        while timer:
            mins, secs = divmod(timer, 60)
            timeformat = "{:02d}:{:02d}".format(mins, secs)
            self.render_text([label, timeformat], clear_all=False)
            time.sleep(1)
            timer -= 1

        print_left = int(self.config.get("Printer", "print_left"))
        if print_left > 0:
            print_left = str(print_left - 1)
            self.config.update("Printer", "print_left", print_left)

        self.button.enable()

    def shutdown(self):
        logger.info("poweroff !")
        cmd = "sudo poweroff"
        p = subprocess.Popen(cmd, shell=True)
        p.wait()
