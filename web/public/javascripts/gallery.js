const socket = io();
const gallery = document.getElementById('gallery');

socket.on('picture:new', photo => {
  console.log(`socket.io receive event "picture:new" with data ${JSON.stringify(photo)}`);

  const html = `
  <div id="${photo.name}">
    <a href="${photo.src}"><img src="${photo.thumbnail}"></a>
  </div>`;
  gallery.insertAdjacentHTML('afterBegin', html);
});

socket.on('picture:delete', photo => {
  console.log(`socket.io receive event "picture:delete" with data ${JSON.stringify(photo)}`);
  const img = document.getElementById(photo.name);
  console.log(img);
  gallery.removeChild(img);
});
socket.on('users:update', data => {
  console.log(`socket.io receive event "users:update" with data ${data}`);
  document.getElementById('nb_user').innerHTML = data;
});

socket.on('pictures:update', data => {
  console.log(`socket.io receive event "pictures:update" with data ${data}`);
  document.getElementById('nb_picture').innerHTML = data;
});
