const fs = require('fs');
const path = require('path');

const express = require('express');
const router = express.Router();
const ini = require('ini');
const debug = require('debug')('pibooth:router:admin');
debug('load routes/admin');

const ROOT_DIR = path.join(__dirname, '..', '..');
const ini_path = path.join(ROOT_DIR, 'pybooth.ini');

router.get('/', (req, res, next) => {
  res.render('admin/', { title: 'Administration' });
});

router.get('/config', (req, res, next) => {
  const default_ini_path = path.join(ROOT_DIR, 'pybooth.sample.ini');
  debug(ini_path);
  debug(path.join(ROOT_DIR, 'pybooth.sample.ini'));

  config = ini.parse(fs.readFileSync(ini_path, 'utf-8'));
  default_config = ini.parse(fs.readFileSync(default_ini_path, 'utf-8'));

  const merged_config = { ...config, ...default_config };
  Object.keys(config).forEach(section => {
    Object.keys(config[section]).forEach(setting => {
      merged_config[section][setting] = config[section][setting];
    });
  });

  res.render('admin/config', { title: 'Pybooth - Admin', merged_config });
});

router.post('/config/update', (req, res, next) => {
  debug(ini_path);
  const config = {};
  for (const key in req.body) {
    const [section, setting] = `${key}`.split('.');
    const value = req.body[key];

    if (!config[section]) {
      config[section] = {};
    }
    config[section][setting] = value;
  }
  fs.writeFileSync(ini_path, ini.stringify(config), { encoding: 'utf8' });

  res.redirect('/admin/config');
});

router.post('/reboot', (req, res, next) => {
  debug('system reboot');
  require('child_process').exec('sudo reboot now', msg => {
    debug(msg);
  });
  res.redirect('/admin');
});

router.post('/shutdown', (req, res, next) => {
  debug('system shutdown');
  require('child_process').exec('sudo shutdown now', msg => {
    debug(msg);
  });
  res.redirect('/admin');
});

router.post('/kill-omxplayer', (req, res, next) => {
  //  stop pngview
  debug('sudo killall -s SIGKILL pngview');
  require('child_process').exec('sudo killall -s SIGKILL pngview', msg => {
    debug(msg);
  });

  //  stop omxplayer
  debug('sudo killall -s SIGKILL omxplayer.bin');
  require('child_process').exec('sudo killall -s SIGKILL pngview', msg => {
    debug(msg);
  });

  //  stop gphoto2
  debug('gphoto2 --set-config viewfinder=0');
  require('child_process').exec('gphoto2 --set-config viewfinder=0', msg => {
    debug(msg);
  });
  res.redirect('/admin');
});

module.exports = router;
