# Pybooth

Transformez votre carte Raspberry PI en photobooth.

La partie photobooth en elle-même est gérée via un programme pygame (python).

En option, un serveur web (NodeJS), accessible via un réseau wifi local, est disponible pour voir les photos prises en live et modifier les réglages.

## Matériel nécessaire

- une carte raspberry pi (https://www.raspberrypi.org/)
- un appareil photo numérique compatible avec la librairie gphoto2
- un écran directement relié à la carte (idéalement en hdmi)
- une imprimante photo (ex. : [Canon Selphy CP1300](https://www.canon.fr/printers/selphy-compact-photo-printers/))

## Présentation technique

L'application python est lancé au démarrage de la carte.

gphoto2 capture une vidéo live de l'appareil et l'enregistre dans un fichier fifo temporaire.

omxplayer est lancé en arrière plan et lit le fichier fifo créer précedemmen pour l'afficher par dessus le programme python.

Lorsque le bouton est appuyé:

- un décompte de 5 secondes est lancé
- la capture live est arrêtée
- une photo est prise
- le rendu de la photo est affiché
- l'utilisateur a 30 secondes pour décider si il souhaite imprimer la photo ou non

On retourne ensuite au début du programme et on attend une nouvelle intéractionv via l'unique bouton poussoir.

## Installation

### Packages

```
sudo apt install -y git htop vim dnsmasq hostapd nginx omxplayer python3-pip libjpeg-dev usbmount
sudo apt install -y xinit matchbox-window-manager unclutter xserver-xorg-video-all xserver-xorg-input-all xserver-xorg-core xinit x11-xserver-utils libpng12-dev

```

- `git` : nécéssaires pour récupérer les sources de différentes applications
- `htop` : utilitaire pour monitorer les ressources utilisées
- `vim` : éditeur de documents
- `dnsmasq` :
- `hostapd` :
- `nginx` :
- `omxplayer` :
- `xinit` :
- `matchbox-window-manager` :

* `unclutter` :

### Kiosk mode & auto start

`.xinitrc`

```shell
#!/bin/sh
xset -dpms
xset s off

unclutter &

matchbox-window-manager -use_cursor no -use_titlebar no &

python3 /home/pi/pybooth/app/Pybooth.py > /home/pi/pybooth/logs/xinit.log
```

`.bashrc`

Ajouter les lignes ci-dessous dans le fichier .bashrc de l'utilisateur "pi" (`/home/pi/.bashrc`)

```shell
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    startx
fi
```

### python

change default python version to python 3

```
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 1
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 2
sudo update-alternatives --config python
```

### gphoto2

```
wget https://raw.githubusercontent.com/gonzalo/gphoto2-updater/master/gphoto2-updater.sh && chmod +x gphoto2-updater.sh && sudo ./gphoto2-updater.sh --stable
sudo pip install -v gphoto2
```

### CUPS

```
sudo apt install -y cups
sudo usermod -a -G lpadmin pi
sudo cupsctl --remote-any
sudo /etc/init.d/cups restart
```

Ajouter l'imprimante en se rendant à l'adresse suivante :
https://<ADRESSE IP>:631/admin

Login/password : pi/raspberry
Model : Canon SELPHY CP910

### create_ap

```
sudo create_ap -n wlan0 --isolate-clients "Pibooth <3"
```

### Rotation de l'écran

`sudo vim /boot/config.txt`

```
#flips the display vertically
display_rotate=0x20000

#flips the display horizontally
display_rotate=0x10000
```

`sudo reboot`

### Overlay

```
git clone https://github.com/AndrewFromMelbourne/raspidmx.git
cd raspidmx
sudo Make
sudo cp pngview/pngview /usr/local/bin
sudo cp lib/libraspidmx.so.1 /usr/local/lib
cd ..
rm -rf raspidmx.git
```

### Nginx

```
server {
	listen 80 default_server;
	listen [::]:80 default_server;

  server_name _;

  location / {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
  }
}
```

### Node

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install -y nodejs
```

Pour résoudre les problèmes de droits pour installer des paquets de façon globale :

```
# créer un dossier dédié
mkdir ~/.npm-global
# Configurer npm pour utiliser ce dossier
npm config set prefix '~/.npm-global'
# Ajouter cette ligne à votre fichier .profile ou .bashrc
export PATH=~/.npm-global/bin:$PATH
# Mettre à jour les variables
source ~/.profile
```

### PM2

```
npm install -g pm2
pm2 completion install
pm2 startup

pm2 start pybooth/web/bin/www --name pybooth-webadmin -i max
pm2 save
```

## Dépendances

- [python 3](https://www.python.org/)
- [gphoto2](http://www.gphoto.org/)
- [ImageMagick](http://www.imagemagick.org/)
- [Node JS](https://nodejs.org/en/)
- [create_ap](https://github.com/oblique/create_ap)
