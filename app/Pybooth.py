#! /usr/bin/env python3
import os
import signal
import subprocess

import pygame

from src.App import App
from src.Config import Config
from src.Logger import logger
from src.utils import ROOT_DIR


def main():
    """
    Start Pybooth from here
        - create a default 'thumbnail' folder into /media/usb
        - load configuration or generate default's one if no config file founded
        - start pygame
        - wait for action on button
    """
    try:
        logger.info("program start")
        os.makedirs("/media/usb/thumbnail/", exist_ok=True)

        config = Config()
        display = pygame.display.set_mode(
            (int(config.get("Screen", "width")), int(config.get("Screen", "height")))
        )

        app = App(display)
        app.loop()

    except FileNotFoundError:
        logger.error("Can't create /media/usb/thumbnail")

    except KeyboardInterrupt:
        logger.info("W: interrupt received, stopping…")

    finally:
        if 1 == config.get("AP", "enable"):
            ssid = config.get("AP", "SSID")
            logger.info("Disable AP '{0}'".format(ssid))
            cmd = "sudo create_ap --stop wlan0"
            p = subprocess.Popen(cmd, shell=True)
        logger.info("See you soon!")


if __name__ == "__main__":
    main()
