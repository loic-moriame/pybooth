import contextlib
import os

import logzero
from logzero import setup_logger, LogFormatter

from src.utils import ROOT_DIR

DEFAULT_LOG_PATH = os.path.join(ROOT_DIR, "../logs/pybooth.log")
DEFAULT_FORMAT = "%(color)s[%(levelname)s %(asctime)s %(module)s:%(lineno)d]%(end_color)s %(message)s"
DEFAULT_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

logger = None


def reset_default_logger():
    global logger

    path_logs = os.path.join(ROOT_DIR, "../logs")
    os.makedirs(path_logs, exist_ok=True)
    with contextlib.suppress(FileNotFoundError):
        os.remove(DEFAULT_LOG_PATH)

    formatter = LogFormatter(fmt=DEFAULT_FORMAT, datefmt=DEFAULT_DATE_FORMAT)
    logger = setup_logger(logfile=DEFAULT_LOG_PATH, formatter=formatter)

    logzero.loglevel(logzero.logging.INFO)


reset_default_logger()

if __name__ == "__main__":
    reset_default_logger()
    logger.info("logger init")
