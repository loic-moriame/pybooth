import configparser
import os

from src.utils import ROOT_DIR
from src.Logger import logger

DEFAULT_PATH = os.path.join(ROOT_DIR, "../pybooth.ini")


class Config:
    def __init__(self, path=DEFAULT_PATH):
        """ Constructor of the class """
        # print(path)
        self.settings_path = path

        try:
            if not os.path.exists(self.settings_path):
                self.create_default_config(self.settings_path)

            config = configparser.ConfigParser()
            config.read(self.settings_path)
            self.config = config
            self.check_required_values()

        except IOError:
            logger.error("Can't create config file")

    def create_default_config(self, path):
        """
        Create a default config file
        """
        config = configparser.ConfigParser()

        config.add_section("Pybooth")
        # config.set("Pybooth", "pin_button_action", "27")
        # config.set("Pybooth", "pin_button_shutdown", "21")
        config.set("Pybooth", "countdown_seconds_shoot", "5")
        config.set("Pybooth", "countdown_seconds_print", "10")
        config.set("Pybooth", "watermark", "0")

        config.add_section("Screen")
        # config.set("Screen", "width", "15")
        # config.set("Screen", "height", "15")
        config.set("Screen", "percent_text_area_display", "25")

        config.add_section("Camera")
        # config.set("Camera", "pin_relay", "15")

        config.add_section("Printer")
        # config.set("Printer", "name", "Canon_SELPHY_CP1300")

        config.add_section("AP")
        config.set("AP", "enable", "1")
        config.set("AP", "SSID", "Pibooth<3")
        config.set("AP", "password", "")

        with open(path, "w") as config_file:
            config.write(config_file)

    def check_required_values(self):
        """
        Validate the config file to be sure that every required values is set
        If required values are not set, set them to her default value
        """
        error_section = 0
        error_option = 0

        """
        Check if required parameter '[Pybooth] pin_button_action' is set
        """
        err_section, err_option = self.validate("Pybooth", "pin_button_action")
        if err_section:
            error_section += 1
        if err_option:
            error_option += 1

        """
        Check if required parameter '[Screen] width' is set
        """
        err_section, err_option = self.validate("Screen", "width")
        if err_section:
            error_section += 1
        if err_option:
            error_option += 1

        """
        Check if required parameter '[Screen] height' is set
        """
        err_section, err_option = self.validate("Screen", "height")
        if err_section:
            error_section += 1
        if err_option:
            error_option += 1

        """
        Check if required parameter '[Printer] name' is set
        """
        err_section, err_option = self.validate("Printer", "name")
        if err_section:
            error_section += 1
        if err_option:
            error_option += 1

        """
        Check if required parameter '[Printer] nb_of_cartridge' is set
        """
        err_section, err_option = self.validate("Printer", "nb_of_cartridge")
        if err_section:
            error_section += 1
        if err_option:
            error_option += 1

        """
        Check if required parameter '[Printer] nb_of_sheet' is set
        """
        err_section, err_option = self.validate("Printer", "nb_of_sheet")
        if err_section:
            error_section += 1
        if err_option:
            error_option += 1

        """
        Check if required parameter '[Printer] total_sheets' is set
        """
        err_section, err_option = self.validate("Printer", "total_sheets")
        if err_section:
            error_section += 1
        if err_option:
            error_option += 1

        """
        Check if parameter '[Printer] print_left' is set only if no local db already set
        """
        if not os.path.isfile(os.path.join(ROOT_DIR, "../db.json")):
            err_section, err_option = self.validate("Printer", "print_left")
            if err_section:
                error_section += 1
            if err_option:
                error_option += 1

            pass

        """
        Check if required parameter "SSID" is set if "AP" is set to "1"
        """
        try:
            value = self.get("AP", "enable")
            if value == "1":
                section = "AP"
                option = "SSID"
                try:
                    value = self.get("AP", "SSID")
                    if not value:
                        logger.error(
                            "AP is set to be enable, but option '{0}' is not set in section ['{1}']".format(
                                option, section
                            )
                        )
                        error_option += 1
                except configparser.NoOptionError:
                    logger.error(
                        "AP is set to be enable, but option '{0}' is not set in section ['{1}']".format(
                            option, section
                        )
                    )
                    error_option += 1
                    pass
        except configparser.NoSectionError:
            pass
        except configparser.NoOptionError:
            pass

        if error_section > 0:
            raise Exception(
                "Missing some required section ({0}) \nCheck logs for more informations".format(
                    error_section
                )
            )
        if error_option > 0:
            raise Exception(
                "Missing some required parameterts ({0}) \nCheck logs for more informations".format(
                    error_option
                )
            )

    def get(self, section, setting):
        """
        Return a setting value
        """
        try:
            value = self.config.get(section, setting)
            return value
        except configparser.NoOptionError:
            return False

    def update(self, section, setting, value):
        """
        Update a setting
        """
        self.config.set(section, setting, value)
        with open(self.settings_path, "w") as config_file:
            self.config.write(config_file)

    def delete(self, section, setting):
        """
        Delete a setting
        """
        self.config.remove_option(section, setting)
        with open(self.settings_path, "w") as config_file:
            self.config.write(config_file)

    def validate(self, section, option):
        error_section = False
        error_option = False

        try:
            value = self.get(section, option)
            if not value:
                raise configparser.NoOptionError(option, section)
        except configparser.NoSectionError:
            logger.error("Section ['{0}'] is not define".format(section))
            error_section = True
            pass
        except configparser.NoOptionError:
            logger.error(
                "Parameter '{0}' is not set in section ['{1}']".format(option, section)
            )
            error_option = True
            pass

        return error_section, error_option


Config()
