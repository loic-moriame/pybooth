const debug = require('debug')('pibooth:router:index');
debug('load routes/index');

const home = require('./home');
const admin = require('./admin');

module.exports = app => {
  app.use('/', home);
  app.use('/admin', admin);
};
